# README #

# N-folds Scripts in Python

scripts to determine possible number of folds when folding a paper either i) alternate side folding or ii) single side folding

+ n-folds.py to check number of folds possible in alternate side folding 

+ n-folds-single.py to check number of folds possible in single side folding

### How do I get set up? ###

+ nothing to setup, just download the scripts and run it using python. need to install python on your machine.

### Contribution guidelines ###

+ all code improvements, additions, suggestions welcome